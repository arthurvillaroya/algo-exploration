#pragma once

#include "node.h"
#include <vector>
#include <iostream>

class Tree{
    public : 
        std::vector<Node> nodes;
        void loadTree(std::string path);
        Node getRoot();
        void addNode(Node node);
        void largeur(std::string goal);
        void profondeur(std::string goal);
};