#include "node.h"
#include "tree.h"

int main(int argc, char* argv[]){

    Tree tree;

    tree.loadTree("tree.txt");

    for(int i = 0; i < tree.nodes.size(); i++){
        tree.nodes[i].display();
    }

    std::string goal;
    std::cout<<"entrez la valeur recherché : "<<std::endl;
    std::cin>>goal;


    tree.largeur(goal);
    return 0;
}