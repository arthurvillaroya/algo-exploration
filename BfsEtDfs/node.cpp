#include "node.h"

Node::Node(){
    this->iD = -1;
    this->value = -1;
    this->idParent = -1;
}

Node::Node(int iD, std::string value, int idParent){
    this->iD = iD;
    this->value = value;
    this->idParent = idParent;
}

Node::Node(int iD, std::string value, const std::vector<int> &children, int idParent){
    this->iD = iD;
    this->value = value;
    this->idParent = idParent;
    this->children = children;
}

int Node::getiD(){
    return this->iD;
}

std::string Node::getValue(){
    return this->value;
}

int Node::getChild(int fils){
	return this->children[fils];
}

std::vector<int> Node::getChildren(){
	return this->children;
}

void Node::newChild(int newChild){
    this->children.push_back(newChild);
}

int Node::getParent(){
    return this->idParent;
}

bool Node::isLeaf(){
	return this->children.size() == 0;
}

void Node::display(){
    std::cout<<"ID : "<<this->iD<<" | Valeur : "<<this->value<<" | Enfants : ";
    displayEnfant();
    std::cout<<" | idParent : "<<this->idParent<<std::endl;
}

void Node::displayEnfant(){
    for(int i = 0; i < this->children.size(); i++){
        std::cout<<"["<<this->children[i]<<"]";
    }
}