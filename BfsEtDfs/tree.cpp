#include "tree.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm> 

void Tree::loadTree(std::string path){
    std::ifstream treeFile;
    treeFile.open(path);

    std::string nodeInfo = "¤ EMPTY";

    std::getline(treeFile, nodeInfo);
    
    while(nodeInfo != "¤ EMPTY"){
        int iD = this->nodes.size();

        std::string value = nodeInfo.substr(0, nodeInfo.find(':'));
        nodeInfo = nodeInfo.substr(nodeInfo.find(':') + 1);

        int idParent = std::stoi(nodeInfo.substr(0, nodeInfo.find(':')));
        nodeInfo = nodeInfo.substr(nodeInfo.find(':') + 1);

        std::vector<int> children;
        size_t pos = 0;
        std::string token = nodeInfo;

        while ((pos = nodeInfo.find(" ")) != std::string::npos) {
            token = nodeInfo.substr(0, pos);
            children.push_back(std::stoi(token));
            nodeInfo.erase(0, pos + 1);
        }

        if(nodeInfo != ""){
            children.push_back(std::stoi(nodeInfo));
        }
        

        this->addNode(Node(iD, value, children, idParent));
        nodeInfo = "¤ EMPTY";
        std::getline(treeFile, nodeInfo);
    }
}

Node Tree::getRoot(){
    return this->nodes[0];
}
void Tree::addNode(Node node){
    this->nodes.push_back(node);
}

void Tree::largeur(std::string goal){
    std::vector<std::string> chemin;
    std::vector<std::string> noeudExploString;
    
    Node currentNode = this->nodes[0];

    noeudExploString.push_back(currentNode.getValue());

    std::vector<int> nextExplore;

    while(goal != currentNode.getValue() && noeudExploString.size() < this->nodes.size()){
        std::vector<int> tempChildren = currentNode.getChildren();
        nextExplore.insert(std::end(nextExplore), std::begin(tempChildren), std::end(tempChildren));

        currentNode = this->nodes[nextExplore[0]];
        nextExplore.erase(nextExplore.begin());
        
        noeudExploString.push_back(currentNode.getValue());
    }
    if(goal != currentNode.getValue()){
        std::cout<<"il n'y a pas la valeur recherché dans l'arbre donné en paramètre"<<std::endl;
    }
    else{
        chemin.push_back(currentNode.getValue());
        while(currentNode.getiD() != 0){
            currentNode = this->nodes[currentNode.getParent()];
            chemin.push_back(currentNode.getValue());
        }
        std::reverse(chemin.begin(), chemin.end());
    }

    std::cout<<"LE CHEMIN PARCOURU : ";
    for(int i = 0; i < chemin.size(); i++){
        std::cout<<"["<<chemin[i]<<"] | ";
    }
    std::cout<<std::endl;

    std::cout<<"LES NOEUDS EXPLORES: ";
    for(int i = 0; i < noeudExploString.size(); i++){
        std::cout<<"["<<noeudExploString[i]<<"] | ";
    }
    std::cout<<std::endl;
}

void Tree::profondeur(std::string goal){
    std::vector<std::string> chemin;
    std::vector<std::string> noeudExploString;
    
    Node currentNode = this->nodes[0];

    noeudExploString.push_back(currentNode.getValue());

    std::vector<int> nextExplore;

    while(goal != currentNode.getValue() && noeudExploString.size() < this->nodes.size()){
        std::vector<int> tempChildren = currentNode.getChildren();
        nextExplore.insert(std::begin(nextExplore), std::begin(tempChildren), std::end(tempChildren));

        currentNode = this->nodes[nextExplore[0]];
        nextExplore.erase(nextExplore.begin());
        
        noeudExploString.push_back(currentNode.getValue());
    }
    if(goal != currentNode.getValue()){
        std::cout<<"il n'y a pas la valeur recherché dans l'arbre donné en paramètre"<<std::endl;
    }
    else{
        chemin.push_back(currentNode.getValue());
        while(currentNode.getiD() != 0){
            currentNode = this->nodes[currentNode.getParent()];
            chemin.push_back(currentNode.getValue());
        }
        std::reverse(chemin.begin(), chemin.end());
    }

    std::cout<<"LE CHEMIN PARCOURU : ";
    for(int i = 0; i < chemin.size(); i++){
        std::cout<<"["<<chemin[i]<<"] | ";
    }
    std::cout<<std::endl;

    std::cout<<"LES NOEUDS EXPLORES: ";
    for(int i = 0; i < noeudExploString.size(); i++){
        std::cout<<"["<<noeudExploString[i]<<"] | ";
    }
    std::cout<<std::endl;
}