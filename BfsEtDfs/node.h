#pragma once

#include <vector>
#include <iostream>

class Node{
    
    private :
        int iD;
        std::string value;
        std::vector<int> children;
        int idParent;

    public : 
        Node();
        Node(int iD, std::string value, int idParent);
        Node(int iD, std::string value, const std::vector<int> &children, int idParent);
        int getiD();
        std::string getValue();
        int getChild(int fils);
        std::vector<int> getChildren();
        int getParent();
        void newChild(int newChild);
        bool isLeaf();
        void display();
        void displayEnfant();
};